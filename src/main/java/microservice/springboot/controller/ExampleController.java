/**
 * 
 */
package microservice.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author vamsikrishna_a
 *
 */
@RestController
public class ExampleController {
	
	@RequestMapping("/")
    String home() {
        return "Hello World!";
    }


}
