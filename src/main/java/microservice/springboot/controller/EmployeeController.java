package microservice.springboot.controller;

import microservice.springboot.domain.Employee;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author vamsikrishna_a
 *
 */
@RestController
public class EmployeeController {
	
	@RequestMapping(value= "/employee", produces={MediaType.APPLICATION_JSON_UTF8_VALUE},  method = RequestMethod.GET )
    public Employee home() {
		Employee e = new Employee();
		e.setId("1233");
		e.setName("Raj");
        return e;
    }


}
